# Website

Website last updated on: July 11 2019

Source code for my personal website <a href="http://alex.waitlisters.org">alex.waitlisters.org</a>. I wrote the website myself except for the <a href="https://github.com/jsoma/tabletop">tabletop</a> (allows HTML content to be drawn from a Google Spreadsheet).
